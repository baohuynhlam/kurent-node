"use strict";

import React, { useEffect } from "react";
import socketIOClient from "socket.io-client";

const socket = socketIOClient("http://localhost:5000");

function emitMessage(sender, data) {
    socket.emit("message", {
        source: sender,
        ...data,
    });
}

class PeerConnection {
    constructor(id) {
        const servers = {
            iceServers: [
                {
                    urls: "stun:172.253.117.127:19302",
                },
            ],
        };
        this.peerConnection = new RTCPeerConnection(servers);
        this.id = id;
    }

    initEventListener(onConnected) {
        this.peerConnection.onicecandidate = (event) => {
            console.log(`${this.id} onicecandidate`, event.candidate);
            if (event.candidate) {
                emitMessage(this.id, {
                    id: "onIceCandidate",
                    candidate: event.candidate,
                });
            }
        };

        this.peerConnection.onicegatheringstatechange = (event) => {
            console.log(
                `${this.id} ice gathering state change`,
                this.peerConnection.iceGatheringState
            );
        };

        this.peerConnection.oniceconnectionstatechange = (event) => {
            const state = this.peerConnection.iceConnectionState;
            console.log(`${this.id} ice connection state change`, state);
            if (state === "disconnected" || state === "failed") {
                this.peerConnection.restartIce();
            } else if (state === "connected") {
                onConnected();
            }
        };
    }

    async createAndSendOffer() {
        const offer = await this.peerConnection.createOffer({
            iceRestart: true,
        });
        this.peerConnection.setLocalDescription(offer);
        emitMessage(this.id, {
            id: "start",
            sdpOffer: offer.sdp,
        });
    }

    close() {
        this.peerConnection.close();
    }
}

function App() {
    useEffect(() => {
        let pc1 = new PeerConnection("screen");
        let pc2 = new PeerConnection("webcam");
        const pcBySource = {
            screen: pc1,
            webcam: pc2,
        };

        const screenInput = document.getElementById("screenInput");
        const webcamInput = document.getElementById("webcamInput");
        const toggleVideoButton = document.getElementById("toggleVideo");
        const startButton = document.getElementById("start");
        const stopButton = document.getElementById("stop");

        let screenStream = new MediaStream();
        let webcamStream = new MediaStream();

        stopButton.onclick = () => {
            console.log("Stopping");

            screenStream.getTracks().forEach((track) => {
                track.stop();
            });
            screenInput.srcObject = null;

            webcamStream.getTracks().forEach((track) => {
                track.stop();
            });
            webcamInput.srcObject = null;

            emitMessage(pc1.id, {
                id: "stopRecord",
            });

            emitMessage(pc2.id, {
                id: "stopRecord",
            });
            pc1.close();
            pc2.close();
        };

        toggleVideoButton.onclick = () => {
            screenStream?.getVideoTracks().forEach((track) => {
                track.enabled = !track.enabled;
            });
        };

        startButton.onclick = async function startConnection() {
            // Set up screen-share peer connection
            pc1.peerConnection.addTransceiver("video", {
                direction: "sendonly",
            });

            screenStream = await navigator.mediaDevices.getDisplayMedia({
                video: true,
            });

            screenInput.srcObject = screenStream;

            screenStream.getTracks().forEach((track) => {
                console.log("pc1 add track", track);
                pc1.peerConnection.addTrack(track, screenStream);
            });

            // Set up webcam peer connection
            pc2.peerConnection.addTransceiver("video", {
                direction: "sendonly",
            });

            pc2.peerConnection.addTransceiver("audio", {
                direction: "sendonly",
            });

            webcamStream = await navigator.mediaDevices.getUserMedia({
                video: true,
                audio: true,
            });

            webcamInput.srcObject = webcamStream;

            webcamStream.getTracks().forEach((track) => {
                console.log("pc2 add track", track);
                pc2.peerConnection.addTrack(track, screenStream);
            });

            const onScreenConnected = () => {
                console.log("CONNNECTED Screen - start Record now");
                emitMessage(pc1.id, {
                    id: "startRecord",
                });
            };

            const onWebcamConnected = () => {
                console.log("CONNECTED Webcam - start record now");
                emitMessage(pc2.id, {
                    id: "startRecord",
                });
            };

            pc1.initEventListener(onScreenConnected);
            pc2.initEventListener(onWebcamConnected);

            console.log("Sending pc1 offer");
            await pc1.createAndSendOffer();

            console.log("Sending pc2 offer");
            await pc2.createAndSendOffer();
        };

        socket.on("error", ({ message }) => {
            console.log("Error from socket server", message);
        });

        socket.on("startResponse", ({ source, sdpAnswer }) => {
            console.log("Answer received; set remote description");

            pcBySource[source].peerConnection.setRemoteDescription(
                new RTCSessionDescription({
                    sdp: sdpAnswer,
                    type: "answer",
                })
            );
            emitMessage(pcBySource[source].id, {
                id: "gatherCandidates",
            });
        });

        socket.on("iceCandidate", ({ source, candidate }) => {
            console.log("socket event iceCandidate - meaning server IceCandidateFound");
            pcBySource[source].peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
        });
    }, []);
    return (
        <React.Fragment>
            <head>
                <meta charSet="utf-8" />
                <meta httpEquiv="cache-control" content="no-cache" />
                <meta httpEquiv="pragma" content="no-cache" />
                <meta httpEquiv="expires" content="0" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />

                <link
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                    crossOrigin="anonymous"
                />

                <script
                    src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                    crossOrigin="anonymous"
                ></script>
                <script
                    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
                    crossOrigin="anonymous"
                ></script>
                <script
                    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
                    crossOrigin="anonymous"
                ></script>

                <script
                    src="https://cdnjs.cloudflare.com/ajax/libs/kurento-client/6.16.0/js/kurento-client.min.js"
                    integrity="sha512-eSbtixs7l1ONKNSqbN5T7DJuqQwOVoEUM1s4zdPAsbgPgl4pPvMRiHnTKVq+iRBTeRUW+lALL33lNpjWH5ROJg=="
                    crossOrigin="anonymous"
                ></script>
                <title>Kurento Tutorial: Recorder</title>
            </head>
            <body>
                <div className="container">
                    <div className="page-header">
                        <h1>Tutorial: Recorder</h1>
                        <p>
                            This application shows a <i>WebRtcEndpoint</i> connected to itself
                            (loopback) with a <i>RecorderEndpoint</i>. Take a look to the
                            <a
                                data-toggle="lightbox"
                                data-title="Recorder Pipeline"
                                data-footer="WebRtcEnpoint in loopback with RecorderEndpoint"
                            >
                                Media Pipeline
                            </a>
                            . To run this demo follow these steps:
                        </p>
                        <ol>
                            <li>
                                Open this page with a browser compliant with WebRTC (Chrome,
                                Firefox).
                            </li>
                            <li>
                                Click on <i>Start</i> button to start the SDP negotiation process.
                            </li>
                            <li>
                                Wait a second for the process to finish. Then click <i>Play</i> to
                                open the webcam and start recording
                            </li>
                            <li>
                                Click on <i>Stop</i> to stop recording and finish the communication.
                                Then check the bind-mounted folder to see the recording
                            </li>
                        </ol>
                    </div>
                    <div className="row">
                        <div className="col-md-5">
                            <h3>Local stream</h3>
                            <video id="screenInput" autoPlay width="400px" height="360px"></video>
                        </div>
                        <div className="col-md-2">
                            <button id="start" className="btn btn-success">
                                Start
                            </button>
                            <br />
                            <br />
                            <button id="stop" className="btn btn-danger">
                                Stop
                            </button>
                            <br />
                            <br />
                            <button id="toggleVideo" className="btn btn-warning">
                                Toggle Video
                            </button>
                        </div>
                        <div className="col-md-5">
                            <h3>Remote stream</h3>
                            <video id="webcamInput" autoPlay width="400px" height="360px"></video>
                        </div>
                    </div>
                </div>
            </body>
        </React.Fragment>
    );
}

export default App;
