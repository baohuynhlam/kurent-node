const Kurento = require("kurento-client");

let sessions = {};
let candidatesQueue = {};

const SOURCE_WEBCAM = "webcam";
const SOURCE_SCREEN = "screen";

function stop(sessionId) {
    if (sessions[sessionId]) {
        for (const source of [SOURCE_WEBCAM, SOURCE_SCREEN]) {
            if (sessions[sesionId][source]) {
                const { pipeline, recorderEndpoint } = sessions[sessionId][source];
                recorderEndpoint?.stop();
                pipeline?.release();
                console.log("Released pipeline of source", source);
            }
        }
    }

    delete sessions[sessionId];
    delete candidatesQueue[sessionId];
}

async function start(sessionId, source, sdpOffer, onIceCandidate) {
    if (!sessionId) {
        return callback(new Error("Cannot use undefined/null sessionId"));
    }

    try {
        const client = await Kurento("ws://localhost:8888/kurento");
        const pipeline = await client.create("MediaPipeline");

        const webRTCep = await pipeline.create("WebRtcEndpoint");

        while (candidatesQueue[sessionId]?.[source]?.length) {
            console.log("Adding candidate from queue");
            const candidate = candidatesQueue[sessionId][source].shift();
            webRTCep.addIceCandidate(candidate);
        }

        webRTCep.on("IceCandidateFound", (event) => {
            if (event.candidate) {
                onIceCandidate(event.candidate);
            }
        });

        webRTCep.on("MediaFlowInStateChange", (event) => {
            console.log("MediaFlowInStateChange", event);
        });

        webRTCep.on("IceComponentStateChange", (event) => {
            console.log("IceComponentStateChange", event);
        });

        const sdpAnswer = await webRTCep.processOffer(sdpOffer);

        // Save into the sessions variable
        if (!sessions[sessionId]) {
            sessions[sessionId] = {};
        }
        sessions[sessionId][source] = {
            pipeline: pipeline,
            webRtcEndpoint: webRTCep,
        };

        return sdpAnswer;
    } catch (err) {
        console.log("kurento start error", err);
    }
}

async function gatherCandidates(sessionId, source) {
    if (sessions[sessionId] && sessions[sessionId][source]) {
        const { webRtcEndpoint } = sessions[sessionId][source];
        console.log("Gather candidates for source", source);
        await webRtcEndpoint?.gatherCandidates();
    }
}

async function startRecord(sessionId, source) {
    if (sessions[sessionId] && sessions[sessionId][source]) {
        const { pipeline, webRtcEndpoint } = sessions[sessionId][source];

        let mediaProfile;
        switch (source) {
            case SOURCE_SCREEN:
                mediaProfile = "WEBM_VIDEO_ONLY";
                break;
            case SOURCE_WEBCAM:
                mediaProfile = "WEBM";
                break;
            default:
                mediaProfile = "WEBM";
                break;
        }
        const recorder = await pipeline.create("RecorderEndpoint", {
            uri: `file:///tmp/${source}.webm`,
            mediaProfile: mediaProfile,
            stopOnEndOfStream: true,
        });
        // Add the enpoint to the session
        sessions[sessionId][source].recorderEndpoint = recorder;

        await webRtcEndpoint.connect(recorder);

        console.log("Start recording");
        await recorder.record();
    }
}

async function stopRecord(sessionId, source) {
    if (sessions[sessionId] && sessions[sessionId][source]) {
        const { recorderEndpoint, pipeline } = sessions[sessionId][source];
        await recorderEndpoint?.stop();
        console.log("Recording stopped");
        pipeline?.release();
    }
}

async function handleIceCandidate(sessionId, source, _candidate) {
    const candidate = Kurento.getComplexType("IceCandidate")(_candidate);

    if (sessions[sessionId]?.[source]) {
        console.log("Adding candidate");
        const currentWebRtcEndpoint = sessions[sessionId][source].webRtcEndpoint;
        currentWebRtcEndpoint.addIceCandidate(candidate);
    } else {
        console.log("Queueing candidate");
        if (!candidatesQueue[sessionId]) {
            candidatesQueue[sessionId] = {};
        }
        if (!candidatesQueue[sessionId][source]) {
            candidatesQueue[sessionId][source] = [];
        }
        candidatesQueue[sessionId][source].push(candidate);
    }
}

module.exports = {
    SOURCE_WEBCAM,
    SOURCE_SCREEN,
    start,
    stop,
    gatherCandidates,
    startRecord,
    stopRecord,
    handleIceCandidate,
};
