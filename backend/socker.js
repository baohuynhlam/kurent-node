const socketio = require("socket.io");
const { sessionMiddleware } = require("./app");

const {
    SOURCE_WEBCAM,
    SOURCE_SCREEN,
    start,
    stop,
    gatherCandidates,
    startRecord,
    stopRecord,
    handleIceCandidate,
} = require("./kurento");

// SOCKET IO MESSAGES TYPES
const START_RESPONSE = "startResponse";
const ERROR = "error";
const ICE_CANDIDATE = "iceCandidate";

function socket(server) {
    const io = socketio(server, {
        cors: { origin: "*" },
        methods: ["GET", "POST"],
    });

    // Middleware to get the request's sessionId
    io.use((socket, next) => {
        sessionMiddleware(socket.request, socket.request.res || {}, next);
    });

    io.on("connection", (socket) => {
        console.log("On connection");
        const sessionId = socket.request.session.id;

        socket.on("error", (error) => {
            console.log(`Connection ${sessionId} error:`, error);
            stop(sessionId);
        });

        socket.on("close", () => {
            console.log(`Connection ${sessionId} closed`);
            stop(sessionId);
        });

        socket.on("message", async (message) => {
            console.log(`Connection ${sessionId} received message:`, message);

            const { id: messageId, source, ...data } = message;

            if (source !== SOURCE_WEBCAM && source !== SOURCE_SCREEN) {
                io.to(socket.id).emit(ERROR, {
                    message: `Invalid source ${source}`,
                });
                return;
            }

            switch (messageId) {
                case "start":
                    try {
                        const onIceCandidate = (candidate) => {
                            io.to(socket.id).emit(ICE_CANDIDATE, {
                                source: source,
                                candidate: candidate,
                            });
                        };
                        const sdpAnswer = await start(
                            sessionId,
                            source,
                            data.sdpOffer,
                            onIceCandidate
                        );
                        io.to(socket.id).emit(START_RESPONSE, {
                            source: source,
                            sdpAnswer: sdpAnswer,
                        });
                    } catch (err) {
                        return io.to(socket.id).emit(ERROR, {
                            message: err.message || err,
                        });
                    }
                    break;

                case "stop":
                    stop(sessionId);
                    break;

                case "onIceCandidate":
                    await handleIceCandidate(sessionId, source, data.candidate);
                    break;

                case "startRecord":
                    startRecord(sessionId, source);
                    break;

                case "stopRecord":
                    stopRecord(sessionId, source);
                    break;

                case "gatherCandidates":
                    await gatherCandidates(sessionId, source);
                    break;

                default:
                    io.to(socket.id).emit(ERROR, {
                        message: `Invalid message ${messageId}`,
                    });
                    break;
            }
        });
    });
}

module.exports = {
    socket,
};
