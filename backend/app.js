const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const session = require("express-session");
const cors = require("cors");

const app = express();

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

app.use(cors());

const sessionMiddleware = session({
    secret: "none",
    rolling: true,
    resave: true,
    saveUninitialized: true,
});
app.use(sessionMiddleware);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "public")));

// app.use("/", indexRouter);
// app.use("/users", usersRouter);

module.exports = {
    app,
    sessionMiddleware,
};
